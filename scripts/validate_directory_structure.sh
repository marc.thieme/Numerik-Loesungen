#!/bin/bash

assert() {
    if ! ${@:1:${#@}-1} ; then
        # "${@:2}" prints all arguments starting from the second one
        echo "Assert failed: ${@:2}" >&2
        exit 1
    fi
}

assert [ $# == 1 ] "Please provide the directory to validate"

loesungen_dir=$1

shopt -s nullglob

echo "Starting validation of the file structure under directory $loesungen_dir"

# Loop through each directory inside 'loesungen'
validated_dir_count=0
for folder in "$loesungen_dir"/*; do
    echo Looking into $folder
    
    assert [ -d "$folder" ] "The $loesungen_dir may only contain folders but we found a file: $folder"

    folder_name=$(basename "$folder")

    # Get all files
    tex_files=$(ls $folder --file-type | grep -v /)

    assert [ "${#tex_files[@]}" == 1 -a "${tex_files[0]}" == "$folder_name.tex" ] "The directory must only contain one .tex file with the same name as its parent folder. However, folder $folder contained files $tex_files"
    
    ((validated_dir_count++))
done

echo "Validated $validated_dir_count folders. Structure was successfully verified..."

shopt -u nullglob