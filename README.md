### Viewing
You can look at the generated pdf solution files in the [artifacts view](https://gitlab.kit.edu/marc.thieme/Numerik-Loesungen/-/jobs/artifacts/main/browse/build?job=compile_pdf).

### Helping
The pipeline automatically builds all .tex files located in a directory under loesungen/. It also enforces the following structure  
```
loesungen/
├── Klausur1
│   ├── Klausur1.tex
│   └── sonstiges...
├── Klausur2
│   ├── Klausur2.tex
│   └── sonstiges...
└── ...
```
Feel free to add exams/tasks/soutions to the project.

The original exam formulations were converted from .pdf to .tex using [mathpix.com](https://mathpix.com/).
