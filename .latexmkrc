$pdf_mode = 1;
$recorder = 1;
$root_dir = cwd();
$out_dir = "$root_dir/build";
$do_cd = 1;
@default_files = glob("loesungen/*/*.tex");
$pdflatex = "pdflatex -output-directory=$out_dir -synctex=1 %O %S";
                                                                                                                                                                                                                 
sub make_build_dir {
    if (!-e $out_dir) {
        mkdir $out_dir or die "Error: could not create build directory $out_dir\n";
    }
}
make_build_dir();
